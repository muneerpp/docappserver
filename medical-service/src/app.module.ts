import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { PingModule } from './ping/ping.module';
import { CaseModule } from './case/case.module';
import { DoctorsModule } from './doctors/doctors.module';
import { UsersModule } from './users/users.module';
import { ConditionOccuranceModule } from './condition-occurance/condition-occurance.module';
import { MedicineReminderModule } from './medicine-reminder/medicine-reminder.module';
import { PrescriptionModule } from './prescription/prescription.module';
import { SymptomModule } from './symptom/symptom.module';
import { MedicalcoreModule } from './medicalcore/medicalcore.module';



@Module({
  imports: [TypeOrmModule.forRoot(typeOrmConfig), PingModule, CaseModule, DoctorsModule, UsersModule, ConditionOccuranceModule, MedicineReminderModule, PrescriptionModule, SymptomModule, MedicalcoreModule],
  controllers: [],
})
export class AppModule { }
