import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { CaseRepository } from "./case.repository";

@Injectable()
export class CaseService {

    constructor(
        @InjectRepository(CaseRepository)
        private caseRepository: CaseRepository
    ) {

    }


    async createCase(): Promise<any> {
        let medicalCase = await this.caseRepository.createCase()
        if (!medicalCase) {
            throw new BadRequestException("Failed to create case")
        }
        return medicalCase
    }
}