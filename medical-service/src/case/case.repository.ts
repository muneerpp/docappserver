import { InternalServerErrorException, NotFoundException } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import { Case } from "./case.entity";

@EntityRepository(Case)
export class CaseRepository extends Repository<Case> {
    async createCase(): Promise<Case> {
        try {
            let medicalCase = new Case()
            medicalCase.userId = 100
            medicalCase.name = "Test case of user"
            medicalCase.startDate = new Date()
            medicalCase.lifelineId = 1
            medicalCase.subscriptionId = 1
            medicalCase.careTeamId = 1
            medicalCase.ownerProfileId = 1
            medicalCase.currentDoctorProfileId =1 
            medicalCase.approvedByProfileId = 1
            medicalCase.forwardedByProfileId = 2
            medicalCase.forwardedByProfileId = 1
            medicalCase.priority = 0
            medicalCase = await medicalCase.save()
            return medicalCase

        } catch (error) {
            throw new InternalServerErrorException(error)
        }
    }
}