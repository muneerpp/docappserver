import { BaseEntity, Column, Entity, PrimaryGeneratedColumn, Unique } from "typeorm";



@Entity()
@Unique(['id'])
export class Case extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    userId: number

    @Column()
    name: string

    @Column()
    startDate: Date

    @Column({ nullable: true })
    updatedDate: string

    @Column({ nullable: true })
    endDate: string

    @Column({ nullable: false, default: "INIT" })
    status: string

    @Column({ nullable: false, default: "DRAFT" })
    workflowState: string

    @Column({ nullable: true })
    notes: string

    @Column({ nullable: false, default: true })
    isOpen: boolean

    @Column()
    lifelineId: number

    @Column()
    subscriptionId: number

    @Column()
    careTeamId: number

    @Column()
    ownerProfileId: number

    @Column()
    currentDoctorProfileId: number

    @Column()
    approvedByProfileId: number

    @Column({ nullable: true })
    forwardedToProfileId: number

    @Column({ nullable: true })
    forwardedByProfileId: number

    @Column()
    priority: number

    @Column({ nullable: false, default: 3 })
    observationPeriod: number

    @Column({ nullable: true })
    observationUnit: string

    @Column({ nullable: false, default: false })
    openedForRework: boolean

    @Column({ nullable: true })
    paymentStatus: string

    @Column({ nullable: false, default: false })
    isArchived: boolean
}