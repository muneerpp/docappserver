import { Controller, Get, Post } from '@nestjs/common';
import { Case } from './case.entity';
import { CaseService } from './case.service';

@Controller('case')
export class CaseController {

    constructor(
        private caseService: CaseService
    ) { }



    @Get('/')
    getCases(): string {
        return "No Cases To Show"
    }


    @Post('/')
    createCase(): Promise<Case> {
        return this.caseService.createCase()
    }
}
