import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CaseController } from './case.controller';
import { CaseRepository } from './case.repository';
import { CaseService } from './case.service';

@Module({
    imports: [TypeOrmModule.forFeature([CaseRepository])],
    controllers: [CaseController],
    providers: [CaseService]
})
export class CaseModule { }
