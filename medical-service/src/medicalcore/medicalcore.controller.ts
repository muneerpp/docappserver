import { Body, Controller, Post } from '@nestjs/common';
import { SymptomDTO } from './dto/symptom.dto';
import { Symptom } from './symptom.entity';
import { SymptomService } from './symptom.service';

@Controller('medicalcore')
export class MedicalcoreController {
    constructor(
        private symptomService: SymptomService
    ) { }



    @Post('/symptom/')
    async createSymptom(
        @Body() symptomDTO: SymptomDTO
    ): Promise<Symptom> {
        return await this.symptomService.createSymptom(symptomDTO)
    }
}
