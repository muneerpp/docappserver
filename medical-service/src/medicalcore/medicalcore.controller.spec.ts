import { Test, TestingModule } from '@nestjs/testing';
import { MedicalcoreController } from './medicalcore.controller';

describe('Medicalcore Controller', () => {
  let controller: MedicalcoreController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MedicalcoreController],
    }).compile();

    controller = module.get<MedicalcoreController>(MedicalcoreController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
