import { InternalServerErrorException, NotFoundException } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import { SymptomDTO } from "./dto/symptom.dto";
import { Symptom } from "./symptom.entity";


@EntityRepository(Symptom)
export class SymptomRepository extends Repository<Symptom> {
    async createSymptom(symptomDTO: SymptomDTO): Promise<Symptom> {
        try {
            let symptom = new Symptom()
            symptom.symptomId = symptomDTO.symptomId
            symptom.name = symptomDTO.name
            symptom.synonyms = symptomDTO.synonyms
            symptom.description = symptomDTO.description
            symptom.isActive = symptomDTO.isActive
            symptom.parentSymptomId = symptomDTO.parentSymptomId
            symptom.observable = symptomDTO.observable
            symptom.metaDataId = symptomDTO.metaDataId
            return await symptom.save()
        } catch (error) {
            throw new InternalServerErrorException(error)
        }
    }
}