import { Test, TestingModule } from '@nestjs/testing';
import { MedicalcoreService } from './medicalcore.service';

describe('MedicalcoreService', () => {
  let service: MedicalcoreService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MedicalcoreService],
    }).compile();

    service = module.get<MedicalcoreService>(MedicalcoreService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
