import { Module } from '@nestjs/common';
import { MedicalcoreService } from './medicalcore.service';
import { MedicalcoreController } from './medicalcore.controller';
import { SymptomService } from './symptom.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SymptomRepository } from './symptom.repository';

@Module({
  imports: [TypeOrmModule.forFeature([SymptomRepository])],
  providers: [MedicalcoreService, SymptomService],
  controllers: [MedicalcoreController]
})
export class MedicalcoreModule { }
