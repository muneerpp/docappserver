import { BaseEntity, Column, Entity, PrimaryGeneratedColumn, Unique } from "typeorm";



@Entity()
@Unique(['id'])
export class Symptom extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    symptomId: string

    @Column()
    name: string

    @Column({ nullable: true })
    synonyms: string

    @Column({ nullable: true })
    description: string

    @Column({ nullable: false, default: true })
    isActive: boolean


    @Column({ nullable: true })
    parentSymptomId: string

    @Column({ nullable: true })
    observable: boolean

    @Column({ nullable: true })
    metaDataId: number
}