export class SymptomDTO {
    id?: number
    symptomId: string
    name: string
    synonyms?: string
    description?: string
    isActive: boolean
    parentSymptomId?: string
    observable: boolean
    metaDataId: number
}