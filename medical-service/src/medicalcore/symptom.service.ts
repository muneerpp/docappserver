import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { SymptomDTO } from "./dto/symptom.dto";
import { Symptom } from "./symptom.entity";
import { SymptomRepository } from './symptom.repository'

@Injectable()
export class SymptomService {

    constructor(
        @InjectRepository(SymptomRepository)
        private symptomRepository: SymptomRepository
    ) {

    }


    async createSymptom(symptomDTO: SymptomDTO): Promise<Symptom> {
        let symptom = await this.symptomRepository.createSymptom(symptomDTO)
        if (!symptom) {
            throw new BadRequestException("Failed to create case")
        }
        return symptom
    }
}